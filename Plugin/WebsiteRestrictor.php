<?php
namespace Vurbis\Punchout\Plugin;

use Magento\WebsiteRestriction\Model\Restrictor;

/**
 * WebsiteRestrictor Plugin
 */
class WebsiteRestrictor
{
    /**
     * Overwrite Restrict method
     *
     * @param Restrictor $subject
     * @param \Vurbis\Punchout\Plugin\callable $proceed
     * @param type $request
     * @param type $response
     * @param type $isCustomerLoggedIn
     * @return type
     */
    public function aroundRestrict(Restrictor $subject, callable $proceed, $request, $response, $isCustomerLoggedIn)
    {
        if ($request->getControllerModule() != 'Vurbis_Punchout') {
            return $proceed($request, $response, $isCustomerLoggedIn);
        }
    }
}
