<?php

namespace Vurbis\Punchout\Controller\Customer;

/**
 * Login controller
 */
class Login extends \Magento\Framework\App\Action\Action
{
    /**
     * Customer session
     * @var Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Vurbis\Punchout\Model\Configuration
     */
    protected $configuration;

    /**
     * @var \Vurbis\Punchout\Model\Punchout
     */
    protected $punchout;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    protected $cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $customerRepository;
    
    /** 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\UrlFactory $urlFactory
     * @param \Vurbis\Punchout\Model\Configuration $configuration
     * @param \Vurbis\Punchout\Model\Punchout $punchout
     * @param \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\UrlFactory $urlFactory,
        \Vurbis\Punchout\Model\Configuration $configuration,
        \Vurbis\Punchout\Model\Punchout $punchout,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->url = $urlFactory->create();
        $this->configuration = $configuration; 
        $this->punchout = $punchout;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Login customer
     */
    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            // log out customer
            if ($this->session->isLoggedIn()) {
                $lastCustomerId = $this->session->getId();
                $this->session->logout()->setLastCustomerId($lastCustomerId);
            }
            $this->session->setPunchoutIsOci(false);
            $req = $this->getRequest();
            $punchoutSession = $req->getParam('session');
            $username = $req->getParam('username');
            $password = $req->getParam('password');
            if (!isset($password)) {
                $password = $req->getParam('pass');
            };
            $apiUrl = $this->configuration->getApiUrl();
            $authenticated = false;
            if (!$punchoutSession) {
                $supplier_id = $this->configuration->getSupplierId();
                $url = $apiUrl . "/punchout/" . $supplier_id . "/login";
                $res = $this->punchout->post($url, [
                    'query' => $req->getParams(),
                    'body' => $req->getPostValue(),
                ]);
                if (!isset($res->id)) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Username and password could not be found in Vurbis marketplace.')
                    );
                }
                $punchoutSession = $res->id;
                $username = $res->username;
                $password = $res->password;
                $authenticated = true;
                $is_clean_customer_id = false;
                if(isset($res->config)) {
                    if(isset($res->config->clean_customer_id)) {
                        $is_clean_customer_id = $res->config->clean_customer_id;
                    }
                }
                $this->session->setPunchoutCleanCustomerId($is_clean_customer_id);
                $this->session->setPunchoutIsOci(true);
            }

            if (!$punchoutSession) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Punchout session is required.'));
            }

            if (!$username) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Username is required.'));
            }
            if (!$password) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Password is required.'));
            }

            if (!$authenticated) {
                $authRes = $this->punchout->post(
                    $apiUrl . "/punchout/authenticate",
                    [
                        'username' => $username,
                        'password' => $password,
                        'session' => $punchoutSession,
                    ]
                );
                if (!$authRes->authenticated) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Authentication failed.'));
                }
            }

            try {
                $customer = $this->customerRepository->get(
                    $username,
                    $this->storeManager->getWebsite()->getWebsiteId()
                );
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Failed to login.'));
            }

            $this->_eventManager->dispatch('customer_data_object_login', ['customer' => $customer]);

            $this->session->setCustomerDataAsLoggedIn($customer);
            $this->session->regenerateId();

            if ($this->cookieManager->getCookie('mage-cache-sessid')) {
                $metadata = $this->cookieMetadataFactory->createCookieMetadata();
                $metadata->setPath('/');
                $this->cookieManager->deleteCookie('mage-cache-sessid', $metadata);
            }

            // Add punchout session ID to customer session
            $this->session->setPunchoutSession($punchoutSession);

            // Fake request method to trigger version update for private content
            $this->_request->setMethod(\Zend\Http\Request::METHOD_POST);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $defaultUrl = $this->url->getUrl('/', ['_secure' => true]);
        $resultRedirect->setUrl($defaultUrl);
        return $resultRedirect;
    }
}
