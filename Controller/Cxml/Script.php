<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Magento\Framework\ObjectManagerInterface;

/**
 * Script controller
 */
class Script extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @var Vurbis\Punchout\Model\Configuration
     */
    protected $configuration;

    /**
     *
     * @var Vurbis\Punchout\Model\Punchout
     */
    protected $punchout;
    
    /**
     *
     * @var Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     *
     * @var Magento\Customer\Model\Session
     */
    protected $session;
    
    /**
     *
     * @var type
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_object;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Vurbis\Punchout\Model\Configuration $configuration
     * @param \Vurbis\Punchout\Model\Punchout $punchout
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Vurbis\Punchout\Model\Configuration $configuration,
        \Vurbis\Punchout\Model\Punchout $punchout,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Customer\Model\Session $session,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->configuration = $configuration;
        $this->punchout = $punchout;
        $this->urlInterface = $urlInterface;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
    }
    
    /**
     * Add script tag to header
     */
    public function execute()
    {
        $sessionId = $this->session->getPunchoutSession();
        if (empty($sessionId)) {
            $response = "function initPunchout(){}//" . time();
        } else {
            $apiUrl = $this->configuration->getApiUrl();
            $cart = $this->checkoutSession->getQuote();
            $url = $apiUrl . '/punchout/files/' . $sessionId . '/magento2.js?proxy=true&cart=' . $cart->getId();
            $response = $this->punchout->get($url);
        }
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        return $result
            ->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0', true)
            ->setHeader('Content-Type', 'application/javascript;charset=UTF-8')
            ->setContents($response);
    }
}
