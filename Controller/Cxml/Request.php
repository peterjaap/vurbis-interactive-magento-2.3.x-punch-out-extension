<?php

namespace Vurbis\Punchout\Controller\Cxml;

/**
 * Request controller
 */
class Request extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Vurbis\Punchout\Model\Configuration
     */
    protected $configuration;
    /**
     * @var Vurbis\Punchout\Model\Punchout
     */
    protected $punchout;
    /**
     * @var Magento\Framework\UrlInterface
     */
    protected $urlInterface;
    /**
     * @var Magento\Framework\Filesystem\Driver\File
     */
    protected $fileSystem;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Vurbis\Punchout\Model\Configuration $configuration
     * @param \Vurbis\Punchout\Model\Punchout $punchout
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\Filesystem\Driver\File $fileSystem
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Vurbis\Punchout\Model\Configuration $configuration,
        \Vurbis\Punchout\Model\Punchout $punchout,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\Filesystem\Driver\File $fileSystem
    ) {
        parent::__construct($context);
        $this->configuration = $configuration;
        $this->punchout = $punchout;
        $this->urlInterface = $urlInterface;
        $this->fileSystem = $fileSystem;
    }
    /**
     * Request action
     */
    public function execute()
    {
        $apiUrl = $this->configuration->getApiUrl();
        $url = $this->urlInterface->getCurrentUrl();
        $path = explode("/punchout/cxml", $url)[1];
        $path = str_replace("/setup", "/request", $path);
        $url = $apiUrl . '/punchout' . $path;
        $body = $this->fileSystem->fileGetContents('php://input');
        $response = $this->punchout->post($url, $body, "xml", "xml");
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        return $result->setHeader('Content-Type', 'text/xml')->setContents($response);
    }
}
