<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Webapi\Rest\Request as ApiRequest;
use Vurbis\Punchout\Model\Configuration;
use Vurbis\Punchout\Model\Punchout;

/**
 * Message Controller
 */
class Message extends Action
{
    /**
     * @var Configuration
     */
    protected $configuration;
    /**
     * @var Punchout
     */
    protected $punchout;
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ApiRequest
     */
    protected $request;

    public function __construct(
        Context $context,
        Configuration $configuration,
        Punchout $punchout,
        Session $session,
        ApiRequest $request
    ) {
        parent::__construct($context);
        $this->configuration = $configuration;
        $this->punchout = $punchout;
        $this->session = $session;
        $this->request = $request;
    }
    /**
     * Add Message script
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public function execute()
    {
        $sessionId = $this->session->getPunchoutSession();
        if (empty($sessionId)) {
            $response = "not a punchout session";
        } else {
            $apiUrl = $this->configuration->getApiUrl();
            $post = json_decode($this->request->getContent(), true);
            $url = $apiUrl . '/punchout/message/' . $sessionId . '?format=magento2-cart&cartId=' . $post['cartId'];
            $response = $this->punchout->post($url, $post, 'json', 'text');
        }
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);

        return $result
            ->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0', true)
            ->setHeader('Content-Type', 'application/json')
            ->setContents($response);
    }
}
