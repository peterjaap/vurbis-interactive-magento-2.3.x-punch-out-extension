# Installation
## Upload module
### Install and update module manually under app/code
If you want to upload the extension manually following these steps:
```
Download the module [here](https://gitlab.com/vurbis/vurbis-interactive-magento-2.3.x-punch-out-extension)
Extract the package
Upload to the {YOUR-MAGENTO2-ROOT-DIR}/app/code/Vurbis/Punchout (If the path does not existing, create it).
After all files and folders are uploaded. Run command to install
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```
### Remove module
```
Remove all files at {YOUR-MAGENTO2-ROOT-DIR}/app/code/Vurbis/Punchout
Open file {YOUR-MAGENTO2-ROOT-DIR}/app/etc/config.php. 
Find and remove the line with Vurbis_Punchout => 1
php bin/magento maintenance:enable
php bin/magento setup:upgrade 
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush 
```
## Composer installation
### Install module
Run composer command in the magento root path

- composer require vurbis/magento2-punchout

After composer.json updated. Run command to install module.
```
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```
### Update plugin
Then run command to update 
```
composer update
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```
### Uninstall
```
composer remove vurbis/magento2-punchout
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```
---

https://www.vurbis.com/

Copyright © 2019 Vurbis. All rights reserved.  

![Vurbis Logo](https://www.vurbis.com/wp-content/uploads/2019/05/VURBIS-INTERACTIVE_trans_logo.png)