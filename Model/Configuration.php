<?php

namespace Vurbis\Punchout\Model;

/**
 * Configuration Model
 */
class Configuration
{
    /**
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }
    /**
     * Get API Url
     *
     * @retun string
     */
    public function getApiUrl()
    {
        $url = $this->__getApiSetting('api_url');
        if (!$url) {
            throw new \Magento\Framework\Exception\LocalizedException(__('API URL is not configured.'));
        }
        return $url;
    }

    /**
     * Get Supplier ID
     *
     * @return string
     */
    public function getSupplierId()
    {
        $supplier_id = $this->__getApiSetting('supplier_id');
        if (!$supplier_id) {
            throw new \Magento\Framework\Exception\LocalizedException(__('SUPPLIER ID is not configured.'));
        }
        return $supplier_id;
    }
    
    
    /**
     * Get api setting value
     *
     * @param string $name
     * @return value
     */
    private function __getApiSetting($name)
    {
        return trim($this->scopeConfig->getValue(
            'vurbis_punchout/api/' . $name,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        ));
    }
}
