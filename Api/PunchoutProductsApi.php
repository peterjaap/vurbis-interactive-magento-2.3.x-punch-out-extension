<?php

namespace Vurbis\Punchout\Api;

/**
 * PunchoutProductsApi Api
 */
class PunchoutProductsApi
{
    /**
     * @var Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;
    /**
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;
    
    /**
     * @var Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var Magento\Framework\App\ResourceConnection
     */
    protected $resource;
     /**
     * @var Vurbis\Punchout\Model\Configuration
     */
    protected $configuration;
    
    const CONNECTION_NAME = 'vurbis';
    /**
     * Constructor
     *
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\App\ResourceConnection $resource,
        \Vurbis\Punchout\Model\Configuration $configuration        
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerRegistry   = $customerRegistry;
        $this->encryptor          = $encryptor;
        $this->resource           = $resource;
        $this->configuration = $configuration;
    }

    /**
     * Get Table name using direct query
     *
     * @param string $tableName
     * @return string
     */
    public function getTablename($tableName)
    {
        /* Create Connection */
        $connection  = $this->resource->getConnection(self::CONNECTION_NAME);
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }

    /**
     * Run
     *
     * @param string $skus
     * @return \Vurbis\Punchout\Api\ApiProductsResponseInterface[]
     * @api
     */
    public function run($skus)
    {
        $results = [];      
        
        $catalog = $this->getTablename('catalog_product_entity');
        $relation = $this->getTablename('catalog_product_relation');

        $row = 'entity_id';
        if ($this->resource->getConnection(self::CONNECTION_NAME)->tableColumnExists($catalog, 'row_id')) {
            $row = 'row_id';
        }

        $skus = explode(";", $skus);        
        foreach ($skus as $childSku) {
            $query = 'SELECT * FROM ' . $catalog . ' parent WHERE parent.' . $row
                . ' IN (SELECT relationship.parent_id FROM ' . $relation
                . ' relationship WHERE relationship.child_id = (SELECT child.entity_id'
                . ' FROM ' . $catalog . ' child WHERE child.sku = :sku)) AND :sku LIKE CONCAT(parent.sku,"%");';
            $bind = [':sku' => (string) $childSku];
            $product = $this->resource->getConnection(self::CONNECTION_NAME)->fetchRow($query, $bind);
            array_push($results, ["sku" => $childSku, "parent" => $product]);
        }
        return $results;
    }
}
