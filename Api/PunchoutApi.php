<?php

namespace Vurbis\Punchout\Api;

/**
 * PunchoutApi Api
 */
class PunchoutApi implements PunchoutApiInterface
{
    /**
     * @var Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;
    /**
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;
    /**
     * @var Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @var Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;
    
    /**
     * @var Vurbis\Punchout\Model\Configuration
     */
    protected $configuration;
    
    const CONNECTION_NAME = 'vurbis';
    /**
     * Constructor
     *
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Vurbis\Punchout\Model\Configuration $configuration    
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerRegistry   = $customerRegistry;
        $this->encryptor          = $encryptor;
        $this->resource           = $resource;
        $this->quoteRepository = $quoteRepository;
        $this->orderRepository = $orderRepository;
        $this->configuration = $configuration;
    }

    /**
     * Run
     *
     * @param string $id
     * @param mixed $props
     * @return \Vurbis\Punchout\Api\ApiUpdateResponseInterface[]
     * @api
     */
    public function run($id, $props)
    {
        $results = [];        
        
        $props = json_decode(json_encode($props), true);
        
        foreach ($props as $prop) {
            $table = $prop['table'];
            $field = $prop['field'];
            $value = $prop['value'];
            $kind  = $prop['kind'];
            $selector = 'entity_id';
            if (isset($prop['selector'])) {
                $selector = $prop['selector'];
            }
            try {
                if ($field == 'password') {
                    $customer       = $this->customerRepository->getById($id);
                    $customerSecure = $this->customerRegistry->retrieveSecureData($id);
                    $customerSecure->setRpToken(null);
                    $customerSecure->setRpTokenCreatedAt(null);
                    $customerSecure->setPasswordHash($this->encryptor->getHash($value, true));
                    $this->customerRepository->save($customer);
                    array_push($results, ["result" => true, "field" => $field, "error" => ""]);
                } elseif ($kind == 'entity_field') {
                    if (!isset($table)) {
                        $table = "customer/entity";
                    }
                    $write = $this->resource->getConnection(self::CONNECTION_NAME);
                    $table = $this->resource->getTableName($table);
                    $data = [];
                    $data[$field] = $value;
                    $key = $selector . ' = ?';
                    $write->update($table, $data, [$key => $id]);
                    array_push($results, [
                        "result" => true,
                        "field" => $field,
                        "error" => "updated table: " . $table
                    ]);
                } elseif ($kind == 'attribute') {
                    if ($table == 'quote') {
                        $entity = $this->quoteRepository->get($id);
                    } elseif ($table == 'order') {
                        $entity = $this->orderRepository->get($id);
                    } else {
                        array_push($results, [
                            "result" => false,
                            "error" => "Table " . $table . " for attribute is not supported",
                            "field" => $field
                        ]);
                        continue;
                    }
                    $extensionAttributes = $entity->getExtensionAttributes();
                    $extensionAttributes->setData($field, $value);
                    $entity->setExtensionAttributes($extensionAttributes);
                    $entity->save();
                    array_push($results, [
                        "result" => true,
                        "field" => $field,
                        "error" => "updated table: " . $table
                    ]);
                } else {
                    array_push($results, [
                        "result" => false,
                        "error" => "Kind " . $kind . " is not supported",
                        "field" => $field
                    ]);
                }
            } catch (Exception $e) {
                array_push($results, [
                    "result" => false,
                    "error" => "exception: " . $e->getMessage(),
                    "field" => $field
                ]);
            }
        }
        return $results;
    }    
}
