<?php

namespace Vurbis\Punchout\Api;

interface PunchoutApiInterface
{
    /**
     * Run
     *
     * @param string $id
     * @param mixed $props
     * @return \Vurbis\Punchout\Api\ApiUpdateResponseInterface[]
     * @api
     */
    public function run($id, $props);
}
